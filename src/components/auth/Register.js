import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

function FormDisabledExample() {
  return (
    <Form>
      <div className='card'>
      <div className='card-header'>
      <h4>Register</h4>
      </div>
        <Form.Group className="mb-3">
          <Form.Label htmlFor="Text">User Name</Form.Label>
          <Form.Control id="Text" placeholder="User Name" />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label htmlFor="Text">Email</Form.Label>
          <Form.Control id="Text" placeholder="Email" />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label htmlFor="Text|Number">Password</Form.Label>
          <Form.Control id="Text" placeholder="Password" />
        </Form.Group>
        <Form.Group className="mb-3">
        </Form.Group>
        <Button type="register">Register</Button>
    </div>
    </Form>
    
  );
}

export default FormDisabledExample;