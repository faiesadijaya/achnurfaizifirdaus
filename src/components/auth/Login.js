import { Form, Button, Container, Row} from "react-bootstrap"
// import Button from 'react-bootstrap/Button';
// import Form from 'react-bootstrap/Form';

const login = () => {
  return (
    <div className="intro">
      <Container className="text-white text-center d-flex justify-content-center align-items-center">
        <Row>
          <Form>
          <div className='card'>
          <div className='card-header'>
          <h4>login</h4>
          </div>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="Text">Email</Form.Label>
              <Form.Control id="Text" placeholder="Email" />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="Text|Number">Password</Form.Label>
              <Form.Control id="Text" placeholder="Password" />
            </Form.Group>
            <Form.Group className="mb-3">
            </Form.Group>
            <Button type="login">Login</Button>
        
            </div>
          </Form>
        </Row>
      </Container>
    </div>
    
 
  
  )
}

export default login
